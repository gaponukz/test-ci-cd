import dataclasses
import typing


@dataclasses.dataclass
class User:
    id: str
    name: str
    age: int
    gender: typing.Optional[float]


if __name__ == "__main__":
    data = {"id": "125121", "name": "Alex Uam", "age": 17, "gender": 51.947}
    user = User(**data)

    print(user)
